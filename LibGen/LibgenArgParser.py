import argparse
import sys


class LibgenArgParser(argparse.ArgumentParser):
    def __init__(self):
        super().__init__()

    def parse_arguments(self):
        if len(sys.argv) == 1:
            self.print_help()
            sys.exit(1)

        group = self.add_mutually_exclusive_group(required=True)

        group.add_argument('-f', '--file', action='store', dest='libgen_download_file', help='File to download from')
        # group.add_argument('-u', '--url', action='store', dest='libgen_url', help='Store a constant value')
        group.add_argument('-s', '--search-term', action='store', dest='libgen_search', help='Search Term - Downloads all books on first page')
        group.add_argument('-t', '--test', action='store_true', dest='libgen_test', help='Test VPN download connectivity')
        group.add_argument('--fix-authors', action='store_true', dest='libgen_fix_authors', help='Fix Missing Authors')

        self.add_argument('-n', '--max-pages', action='store', dest='libgen_max_pages', type=int, help='Max pages - Used only with search term')
        self.add_argument('--starting-page', action='store', dest='libgen_starting_page', type=int, help='Starting page - Used only with search term to indicate which page to start on')
        self.add_argument('--version', action='version', version='%(prog)s 1.0')
        self.add_argument('--strict', action='store_true', dest='libgen_strict_search', help='Strict Search - Search string must be in book title')
        self.add_argument('--redownload', action='store_true', dest='libgen_redownload', help='Force redownload if books exist. Overwrites DB and local file checks.')

        return self.parse_args()

    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)
