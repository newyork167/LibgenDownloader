import hashlib
import os
import PyPDF2
import requests
from bs4 import BeautifulSoup
from clint.textui import progress
import Utilities
from Libgen import LibgenDatabase, LibGenException, LibgenDownloader, LibgenBibtex
from Utilities import utilities, configuration as config
from Utilities.nord_proxy import NordProxy


class LibgenBook:
    id = 0
    author = ""
    title = ""
    publisher = ""
    year = 0
    pages = 0
    language = ""
    extension = ""
    download_links = []
    initial_link = ""

    download_path = ""
    search_term = ""
    libgen_proxy = NordProxy()

    def __init__(self, id_, author, title, publisher, year, pages, language, extension, download_links, search_term):
        self.id = id_
        self.author = author
        self.title = str(title).title()
        self.publisher = publisher
        self.year = year if utilities.check_int(year) else 0
        self.pages = pages
        self.language = language
        self.extension = extension
        self.download_links = download_links
        self.search_term = search_term

        if len(self.download_links):
            self.initial_link = self.download_links[0]

        self.setup_download_path()

    def setup_download_path(self):
        try:
            self.download_path = config.get('libgen', 'download_path')
        except Exception:
            utilities.print_color("Could not find download path in config file, defaulting to './Downloads/'", 'red')
            self.download_path = "Downloads/"

        # Fix separator at end of folder for file creation
        if self.download_path[-1] != os.sep:
            self.download_path += os.sep

        download_path = self.download_path + config.get('libgen', 'search_downloads_path') + os.sep + self.search_term + os.sep

        # Create the folder if it doesn't exist
        self.download_path = utilities.create_path_if_not_exist(download_path)

    def download(self):
        pass

    def write_info_to_db(self, download_path, dl_link, initial_link):
        LibgenDatabase.insert_download(
            ID=self.id,
            title=self.title.strip(),
            author=self.author.strip(),
            search_term=self.search_term,
            location=download_path,
            download_url=dl_link,
            initial_link=initial_link,
            mirrors=";".join(self.download_links),
            pages=self.pages,
            book_year=self.year,
            publisher=self.publisher
        )

    def download_book(self, redownload_existing=False):
        utilities.print_color("Downloading: {}".format(self.title), 'blue')

        # Sanitize the file name for saving
        self.title = utilities.sanitize_file_path(self.title)

        # Build the download path, stripping out any whitespace
        download_path = '{download_path}{title}.{type}'.format(download_path=self.download_path.strip(), title=self.title.title().strip(), type=self.extension.strip())

        # If the file already exists check the md5 and re-download if it doesn't match
        download_exists_in_storage_location = False if redownload_existing else self.check_for_duplicate_download()
        if not (utilities.file_exists_at_path(download_path) or download_exists_in_storage_location) or redownload_existing:
            for dl_link in self.download_links:
                try:
                    if 'libgen.pw' in dl_link:
                        dl_link = self.libgen_pw_download(libgen_link=dl_link)
                    elif 'booksdescr.org' in dl_link:
                        dl_link = self.libgen_booksdescr_download(libgen_link=dl_link)
                    elif 'lib1.org' in dl_link:
                        dl_link = self.libgen_lib1_download(libgen_link=dl_link)

                    utilities.print_color("Attempting download of: {} from {}".format(self.title, dl_link), 'blue')
                    self.download_with_progress(dl_link=dl_link, download_path=download_path, libgen_proxy=self.libgen_proxy)
                    if self.check_for_bad_download(download_path=download_path):
                        raise Exception("Bad File Size")
                    self.write_info_to_db(download_path=download_path, dl_link=dl_link, initial_link=self.initial_link)
                    return LibgenDownloader.LibgenDownloader.DOWNLOAD_SUCCESSFUL
                except Exception as ex:
                    # If an exception occurs for this download link
                    continue
        else:
            LibgenDatabase.update_download(self)
        return LibgenDownloader.LibgenDownloader.DOWNLOAD_FAILED

    def libgen_pw_download(self, libgen_link):
        html = LibgenDownloader.LibgenDownloader.libgen_get(libgen_link, close_response=True, libgen_proxy=self.libgen_proxy)
        soup = BeautifulSoup(html, "html.parser")

        utilities.print_color("Found outer html link, diving deeper!", 'green')
        dl_link = LibgenDownloader.LibgenDownloader.libgen_get_dl_link(soup=soup)

        return dl_link

    def libgen_booksdescr_download(self, libgen_link):
        html = LibgenDownloader.LibgenDownloader.libgen_get(libgen_link, close_response=True, libgen_proxy=self.libgen_proxy)
        soup = BeautifulSoup(html, "html.parser")

        utilities.print_color("Found outer html link, diving deeper!", 'green')
        dl_link = [a['href'] for a in soup.findAll('a') if 'booksdl.org/get.php' in a['href']][0]

        return dl_link

    def libgen_lib1_download(self, libgen_link):
        html = LibgenDownloader.LibgenDownloader.libgen_get(libgen_link, close_response=True, libgen_proxy=self.libgen_proxy)
        soup = BeautifulSoup(html, "html.parser")

        utilities.print_color("Found outer html link, diving deeper!", 'green')
        dl_link = [a['href'] for a in soup.findAll('a') if 'library1.org' in a['href']][0]

        return dl_link

    @staticmethod
    def download_with_progress(dl_link, download_path, libgen_proxy):
        LibgenDownloader.LibgenDownloader.avoid_detection(2, 5)

        try:
            response = LibgenDownloader.LibgenDownloader.libgen_get(dl_link, stream=True, libgen_proxy=libgen_proxy)
        except requests.exceptions.ConnectionError as ex:
            response = LibgenDownloader.LibgenDownloader.libgen_get(dl_link, stream=True, force_no_proxy=True)

        try:
            with open(download_path, 'wb+') as output_file:
                output_file.write(response.content)

                # total_length = int(response.headers.get('content-length'))
                # for chunk in progress.bar(response.iter_content(chunk_size=1024), expected_size=(total_length / 1024) + 1):
                #     if chunk:
                #         output_file.write(chunk)
                #         output_file.flush()
        except Exception as ex:
            utilities.print_color(ex, 'red')

            if os.path.exists(download_path):
                os.remove(download_path)
            raise ex

    @staticmethod
    def check_md5(download_path, md5):
        if md5:
            md5_hash = hashlib.md5(open(download_path, 'rb').read()).hexdigest()

            if md5_hash != md5:
                raise LibGenException("Bad MD5 {} != {}".format(md5, md5_hash))

        return True

    @staticmethod
    def check_for_bad_download(download_path):
        try:
            # First check if the file is a valid pdf file
            try:
                PyPDF2.PdfFileReader(open(download_path, "rb"))
            except Exception as ex:
                Utilities.utilities.print_color("Invalid PDF file")
                return True

            # If it is check for a minimum file size
            if os.path.getsize(download_path) < 5000:
                raise os.error()
        except os.error:
            return True
        return False

    def check_for_duplicate_download(self):
        try:
            sql = """SELECT * FROM libgen.downloads WHERE id = %s OR initial_link = %s"""

            with LibgenDatabase.get_sql_connection() as cursor:
                cursor.execute(sql, (self.id, self.initial_link,))
                return cursor.fetchone()
        except Exception as ex:
            utilities.print_color('Failed to select from database: {}'.format(ex))
            return None

    def get_bibtex(self, soup):
        bibtex_str = soup.find('textarea', {'name': 'bibtext'}).text.strip()
        return self.get_bibtex_from_string(bibtex_str=bibtex_str)

    @staticmethod
    def get_bibtex_from_string(bibtex_str):
        bibtex_dict = LibgenBibtex.parse_bibtex(bibtex_str)
        return bibtex_dict.entries_dict[next(iter(bibtex_dict.entries_dict))]
