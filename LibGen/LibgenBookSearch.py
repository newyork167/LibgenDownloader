import math
import requests
from Libgen.LibgenBook import LibgenBook
from Libgen import LibgenDownloader, LibgenError
from Utilities import utilities, configuration as config
from bs4 import BeautifulSoup

from Utilities.nord_proxy import NordProxy


class LibGenBookSearch:
    base_search_url = 'http://libgen.io/search.php?&req={search_term}&res=100&phrase=1&view=simple&column=def&sort=year&sortmode=DESC{page_num}'
    base_search_url_page_num = '&page={page_num}'
    strict_search = False
    topics = {
        'Medicine': 'topicid147'
    }

    def __init__(self, strict_search=False):
        self.strict_search = strict_search

    @staticmethod
    def libgen_urlify(in_string):
        return "+".join(in_string.split())

    def get_max_pages_from_libgen(self, search_term):
        search_url = self.base_search_url.format(search_term=self.libgen_urlify(search_term), page_num='')
        bs = BeautifulSoup(requests.get(search_url, proxies=NordProxy().get_next_available_proxy()).text, features="html.parser")

        number_of_books = int(bs.find('font', {'color': 'grey', 'size': '1'}).text.split('books found')[0])

        return math.ceil(number_of_books / 100)

    def get_all_books_from_search(self, search_term, page_num, libgen_proxy):
        search_url = self.base_search_url.format(
            search_term=self.libgen_urlify(search_term),
            page_num=self.base_search_url_page_num.format(page_num=page_num) if page_num > 0 else ''
        )
        utilities.print_color("Searching for books in {}".format(search_url))

        html = LibgenDownloader.LibgenDownloader.libgen_get(search_url, close_response=True, libgen_proxy=libgen_proxy)
        soup = BeautifulSoup(html, "html.parser")

        LibgenDownloader.LibgenDownloader.avoid_detection(3, 5)

        data_tables = soup.findChildren('table', {'class': 'c'})
        books_to_download = self.get_book_links_from_data_table(data_tables=data_tables, search_term=search_term)

        utilities.print_color("Downloading: {}".format([b.title for b in books_to_download]))

        return books_to_download

    def check_for_topic_translation(self, topic):
        if topic in self.topics.values():
            return list(self.topics.keys())[list(self.topics.values()).index(topic)]
        return topic

    def get_book_links_from_data_table(self, data_tables, search_term):
        books_to_download = []
        data_table_header = None
        search_term = self.check_for_topic_translation(search_term) if 'topic' in search_term else search_term

        for data_table in data_tables:
            rows = data_table.findChildren(['th', 'tr'], recursive=False)

            for r in range(len(rows)):
                row = rows[r]

                if r == 0:
                    data_table_header = self.parse_search_header_table(table_header=row)
                    continue

                cells = row.findChildren('td')

                try:
                    self.get_book_information(cells=cells, data_table_header=data_table_header)
                    file_id, file_authors, file_title, file_publisher, file_extension, file_language, file_year, file_pages = self.get_book_information(cells=cells, data_table_header=data_table_header)

                    if self.strict_search and str(search_term).lower() not in str(file_title).lower():
                        raise Exception("Skipping {} because strict search failed for search: {}".format(file_title, search_term))

                    if file_extension in config.get('libgen', 'filetypes').lower() and file_language in config.get('libgen', 'languages').lower():
                        # libgen_link = cells[data_table_header['Mirrors']].findAll('a')[0]['href']
                        libgen_links = [c['href'] for c in cells[data_table_header['Mirrors']].findAll('a')]

                        try:
                            book = LibgenBook(
                                id_=file_id,
                                author=file_authors,
                                title=file_title,
                                publisher=file_publisher,
                                extension=file_extension,
                                language=file_language,
                                year=file_year,
                                pages=file_pages,
                                download_links=libgen_links,
                                search_term=search_term
                            )

                            books_to_download.append(book)
                        except Exception as ex:
                            raise Exception('Bad Book Constructor')
                    else:
                        utilities.print_color("Skipping {} since it's not a file we want".format(file_title), 'blue')
                except Exception as ex:
                    utilities.print_color(ex, 'red')

        return books_to_download

    @staticmethod
    def get_book_information(cells, data_table_header):
        file_id = cells[data_table_header['ID']].string.lower().strip()
        file_authors = cells[data_table_header['Author(s)']].text.strip()
        file_title = [x.text.split('<font>')[0] for x in cells[data_table_header['Title']].findAll('a') if 'book' in x['href']][0].strip()
        file_title = [str(x).split('<font')[0].split('>')[-1] for x in cells[data_table_header['Title']].findAll('a') if 'book' in x['href']][0].strip()

        if not file_title:
            file_title = [x.text.split('<font>')[0] for x in cells[data_table_header['Title']].findAll('a') if 'book' in x['href']][0].strip()

        # file_title = [x.text.split(x.find('font').text)[0] for x in cells[data_table_header['Title']].findAll('a') if 'book' in x['href']][0].strip()
        file_title = "{file_title} - {id_}".format(file_title=file_title, id_=file_id)
        file_publisher = cells[data_table_header['Publisher']].text.lower().strip()
        file_extension = cells[data_table_header['Extension']].text.lower().strip()
        file_language = cells[data_table_header['Language']].text.lower().strip()
        file_year = cells[data_table_header['Year']].text.lower().strip()

        try:
            file_pages = cells[data_table_header['Pages']].text.lower().strip().split('[')[0]
            file_pages = [int(s) for s in file_pages.split() if s.isdigit()][0]
        except Exception as ex:
            file_pages = 0

        # l = locals()
        # return {key: l[key] for key in locals().keys() if 'file_' in key}
        return file_id, file_authors, file_title, file_publisher, file_extension, file_language, file_year, file_pages

    @staticmethod
    def parse_search_header_table(table_header):
        return {k: v for v, k in enumerate([x.string for x in table_header.findAll('td')])}

    @staticmethod
    def parse_authors_from_html(author_list):
        return [a.text for a in BeautifulSoup(author_list).findAll('a')]
