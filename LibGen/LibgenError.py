from multiprocessing import Lock
from Utilities import utilities, configuration as config


class LibgenError:
    outputThreading = Lock()

    def write_error_to_output(self, s):
        self.outputThreading.acquire()
        try:
            output_file_path = utilities.create_path_if_not_exist(config.get('libgen', 'error_output_file'), False)
            with open(output_file_path, 'a+') as output_file:
                output_file.write(s.rstrip() + '\n')
        except Exception as ex:
            utilities.print_color("Error writing to output file: {}".format(ex))
        finally:
            self.outputThreading.release()

    def setup_error_output_file(self):
        self.outputThreading.acquire()
        try:
            output_file_path = utilities.create_path_if_not_exist(config.get('libgen', 'error_output_file'), False)
            with open(output_file_path, 'w+') as output_file:
                output_file.write("")
        except Exception as ex:
            utilities.print_color("Error creating error file: {}".format(ex))
        finally:
            self.outputThreading.release()
