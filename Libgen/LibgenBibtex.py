from bibtexparser.bparser import BibTexParser
import bibtexparser


class LibgenBibtex:
    @staticmethod
    def parse_bibtex(bibtex_str):
        parser = BibTexParser()
        parser.ignore_nonstandard_types = False
        parser.homogenize_fields = False
        parser.common_strings = False
        bib_database = bibtexparser.loads(bibtex_str, parser)

        return bib_database
