from multiprocessing.pool import Pool
import MySQLdb
from Utilities import utilities, configuration as config
from Utilities.nord_proxy import NordProxy


def get_sql_connection():
    return MySQLdb.connect(
        host=config.get('database', 'hostname'),
        user=config.get('database', 'username'),
        passwd=config.get('database', 'password'),
        db=config.get('database', 'database'),
        port=config.getint('database', 'port'),
        charset='utf8'
    )


def insert_download(ID, title, author, search_term, location, download_url, initial_link, mirrors, pages, book_year, publisher):
    try:
        sql = """INSERT INTO 
        libgen.downloads(ID, title, author, search_term, location, download_url, initial_link, mirrors, pages, book_year, publisher) 
        VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""

        with get_sql_connection() as cursor:
            cursor.execute(sql, (ID, title, author, search_term, location, download_url, initial_link, mirrors, pages, book_year, publisher,))
            cursor.connection.commit()
    except Exception as ex:
        utilities.print_color('Failed to insert download to database: {}'.format(ex))


def update_download(libgen_book):
    try:
        sql = """UPDATE libgen.downloads 
                    SET 
                        ID = %s, 
                        title = %s, 
                        author = %s, 
                        search_term = %s, 
                        location = %s, 
                        download_url = %s, 
                        initial_link = %s, 
                        mirrors = %s, 
                        pages = %s, 
                        book_year = %s, 
                        publisher = %s
                    WHERE initial_link = %s"""

        with get_sql_connection() as cursor:
            cursor.execute(sql, (
                libgen_book.id,
                libgen_book.title.strip(),
                libgen_book.author.strip(),
                libgen_book.search_term,
                libgen_book.download_path,
                libgen_book.dl_link,
                libgen_book.initial_link,
                ";".join(libgen_book.download_links),
                libgen_book.pages,
                libgen_book.year,
                libgen_book.publisher,
                libgen_book.initial_link,)
            )
            cursor.connection.commit()
    except Exception as ex:
        utilities.print_color('Failed to insert download to database: {}'.format(ex))


def insert_error(download_url, error):
    try:
        sql = """INSERT INTO libgen.error(download_url, error) VALUES(%s, %s)"""

        with get_sql_connection() as cursor:
            cursor.execute(sql, (download_url, error,))
            cursor.connection.commit()
    except Exception as ex:
        utilities.print_color('Failed to insert error to database: {}'.format(ex))


def check_for_duplicate_download(download_url):
    try:
        sql = """SELECT location FROM libgen.downloads WHERE download_url = %s"""

        with get_sql_connection() as cursor:
            cursor.execute(sql, (download_url,))
            return cursor.fetchone()
    except Exception as ex:
        utilities.print_color('Failed to select from database: {}'.format(ex))
        return None


def check_for_duplicate_download_by_initial_link(initial_link):
    try:
        sql = """SELECT * FROM libgen.downloads WHERE initial_link = %s"""

        with get_sql_connection() as cursor:
            cursor.execute(sql, (initial_link,))
            return cursor.fetchone()
    except Exception as ex:
        utilities.print_color('Failed to select from database: {}'.format(ex))
        return None


def update_download_initial_link_by_download_url(initial_link, download_url):
    try:
        sql = """UPDATE libgen.downloads SET initial_link = %s WHERE download_url = %s"""

        with get_sql_connection() as cursor:
            cursor.execute(sql, (initial_link, download_url,))
            cursor.connection.commit()
            return True
    except Exception as ex:
        utilities.print_color('Failed to select from database: {}'.format(ex))
        return False


def fix_authors():
    try:
        sql = """SELECT idx, initial_link, title FROM libgen.downloads WHERE initial_link IS NOT NULL AND author IS NULL"""

        with get_sql_connection() as cursor:
            cursor.execute(sql, )

            records = [r for r in cursor.fetchall()]

            with Pool(5) as thread_pool:
                thread_pool.map(fix_author, records)
                thread_pool.close()
                thread_pool.join()

    except Exception as ex:
        utilities.print_color('Failed to select from database: {}'.format(ex))
        return False


def fix_author(record):
    from bs4 import BeautifulSoup
    import requests
    from main import LibgenDownloader

    libgen_downloader = LibgenDownloader()
    nord_proxy = NordProxy().get_next_available_proxy()

    if nord_proxy:
        utilities.print_color('Using proxy: {}'.format(nord_proxy['http'].split('@')[-1]))
    else:
        utilities.print_color('Using IP')

    idx = record[0]
    initial_link = record[1]
    title = record[2]

    try:

        print(idx, initial_link)

        soup = BeautifulSoup(requests.get(initial_link, proxies=nord_proxy).text, features="html.parser")
        dl_link = libgen_downloader.libgen_get_dl_link(soup=soup)
        libgen_downloader.avoid_detection()
        soup = BeautifulSoup(requests.get(libgen_downloader.base_url + dl_link, proxies=nord_proxy).text, features="html.parser")
        bibtex = libgen_downloader.get_bibtex(soup=soup)

        book_title = utilities.sanitize_file_path(bibtex['title']).strip()
        author = bibtex['author']

        if title != book_title:
            utilities.print_color(
                'Skipping for now because titles were different: {} != {}'.format(title, book_title))
            return

        try:
            sql = """UPDATE libgen.downloads SET author = %s WHERE idx = %s"""

            with get_sql_connection() as cursor:
                cursor.execute(sql, (author, idx,))
                cursor.connection.commit()
        except Exception as ex:
            utilities.print_color('Failed to update record in database: {}'.format(ex))

        libgen_downloader.avoid_detection()
    except Exception as ex:
        utilities.print_color((idx, initial_link, ex))
