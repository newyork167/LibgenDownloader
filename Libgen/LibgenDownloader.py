import os
from multiprocessing import Lock, Pool
import time
from bs4 import BeautifulSoup
import requests
import hashlib
from Libgen import LibgenDatabase, LibGenException
from Libgen.LibgenBibtex import LibgenBibtex
from Libgen.LibgenBook import LibgenBook
from Libgen.LibgenBookSearch import LibGenBookSearch
from Libgen.LibgenError import LibgenError
from Utilities import utilities, configuration as config
from random import randint, shuffle
from clint.textui import progress
from Utilities.nord_proxy import NordProxy

max_threads = min(len(config.get('proxy', 'proxy_urls').split(',')) + 1, config.getint('libgen', 'max_threads'))

files_downloaded = 0


# noinspection PyBroadException
class LibgenDownloader:
    base_url = 'https://libgen.pw'
    base_search_url = '{search_url}/search.php?&req={search_term}&res=100&phrase=1&view=simple&column=def&sort=year&sortmode=DESC{page_num}'
    base_search_url_page_num = '&page={page_num}'
    SKIPPED_DOWNLOAD = 1
    DOWNLOAD_SUCCESSFUL = SKIPPED_DOWNLOAD + 1
    DOWNLOAD_FAILED = DOWNLOAD_SUCCESSFUL + 1
    PAGE_EMPTY = DOWNLOAD_FAILED + 1
    FORCE_NO_PROXY = 'force_no_proxy'
    libgen_error_handler = LibgenError()
    redownload_existing = False
    strict_search = False

    def __init__(self, strict_search=False, redownload_existing=False):
        self.libgen_error_handler.setup_error_output_file()
        self.strict_search = strict_search
        self.redownload_existing = redownload_existing

    def libgen_pool_search_replacement(self, search_term, max_pages=1, starting_page=1, libgen_proxy=NordProxy()):
        # Setup download path - Store in searches folder with search term as folder name
        self.setup_download_path(search_term=search_term)

        num_pages = int(max_pages)
        books_to_download = self.libgen_pool_get_all_books(
            search_term=search_term, num_pages=num_pages, libgen_proxy=libgen_proxy, starting_page=starting_page)

        with Pool(max_threads) as thread_pool:
            # Multiprocess the downloads
            successful_download_count = thread_pool.map(self.libgen_pool_worker_replacement, books_to_download)

            thread_pool.close()
            thread_pool.join()

        return successful_download_count

    @staticmethod
    def libgen_pool_worker_replacement(book, redownload_existing=False):
        try:
            return book.download_book(redownload_existing)
            # return self.multiprocess_download(book=book, search_term=search_term, libgen_proxy=libgen_proxy)
        except Exception as ex:
            utilities.print_color('An Exception occurred with multiprocess worker: {}'.format(ex).capitalize(), 'red')
        finally:
            utilities.print_color('Worker finished')

    def libgen_pool_get_all_books(self, search_term, num_pages, libgen_proxy, starting_page=1):
        libgen_book_search = LibGenBookSearch(strict_search=self.strict_search)
        max_pages = libgen_book_search.get_max_pages_from_libgen(search_term=search_term)

        num_pages = min(num_pages, max_pages)

        # Find all the books on the pages
        with Pool(max_threads) as thread_pool:
            # Multiprocess searches first
            searches_to_make = list(map(lambda e: (search_term, e, libgen_proxy), [page_num for page_num in range(starting_page, num_pages + 1)]))
            books_to_download = thread_pool.starmap(libgen_book_search.get_all_books_from_search, searches_to_make)
            books_to_download = [item for sublist in books_to_download for item in sublist]

            thread_pool.close()
            thread_pool.join()

        # Filter them down to get rid of ones we have already downloaded previously
        with Pool(max_threads) as thread_pool:
            # Check each book's initial link to see if we have already attempted to download this unless we are forcing redownloading of existing books
            books_to_download = thread_pool.map(self.check_book_by_initial_link, books_to_download) if not self.redownload_existing else books_to_download
            books_to_download = [book for book in books_to_download if book is not None]

            # Add some randomness to make detection harder
            shuffle(books_to_download)

        return books_to_download

    def parse_file(self, libgen_file):
        with open(libgen_file, 'r') as lf:
            libgen_link_list = lf.readlines()
            shuffle(libgen_link_list)

            books_to_download = [l.strip() for i, l in enumerate(libgen_link_list)]

            with Pool(max_threads) as thread_pool:
                # Multiprocess the downloads
                successful_download_count = thread_pool.map(self.libgen_pool_worker_replacement, books_to_download)

                thread_pool.close()
                thread_pool.join()

            return successful_download_count

    @staticmethod
    def libgen_get_dl_link(soup):
        dl_link = soup.find('div', {'class': 'book-info__download'}).find('a')['href']
        return dl_link

    def get_download_link(self, soup_str):
        download_link = soup_str.find('div', {'class': 'book-info__get'}).find('a')['href']
        if 'libgen.io' in download_link:
            return download_link
        return self.base_url + download_link

    def get_bibtex(self, soup):
        bibtex_str = soup.find('textarea', {'name': 'bibtext'}).text.strip()
        return self.get_bibtex_from_string(bibtex_str=bibtex_str)

    @staticmethod
    def get_bibtex_from_string(bibtex_str):
        bibtex_dict = LibgenBibtex.parse_bibtex(bibtex_str)
        return bibtex_dict.entries_dict[next(iter(bibtex_dict.entries_dict))]

    @staticmethod
    def avoid_detection(random_sleep_min=5, random_sleep_max=10):
        # Sleep for a random number of seconds so libgen doesn't detect us
        random_sleep_time = randint(random_sleep_min, random_sleep_max)
        utilities.print_color("Sleeping for {} seconds to avoid detection".format(random_sleep_time), 'blue')
        time.sleep(random_sleep_time)

    @staticmethod
    def libgen_get(url, close_response=False, **kwargs):
        if 'libgen_proxy' in kwargs.keys():
            p_object = kwargs['libgen_proxy']
            proxy_object = p_object.get_next_available_proxy()
            kwargs.pop('libgen_proxy', None)
        else:
            proxy_object = NordProxy().get_next_available_proxy()

        # Add some randomness to get our own external IP sometimes
        if proxy_object and LibgenDownloader.FORCE_NO_PROXY not in kwargs.keys():
            utilities.print_color('GET - Using proxy: {}'.format(proxy_object['http'].split('@')[-1]), 'blue')
            if close_response:
                response = requests.get(url, proxies=proxy_object, **kwargs)
                response_text = response.text
                response.close()
                return response_text
            return requests.get(url, proxies=proxy_object, **kwargs)
        else:
            if LibgenDownloader.FORCE_NO_PROXY in kwargs.keys():
                utilities.print_color('GET - Forcing our IP to download', 'blue')
            else:
                utilities.print_color('GET - Using our IP to download', 'blue')
            kwargs.pop(LibgenDownloader.FORCE_NO_PROXY, None)
            if close_response:
                response = requests.get(url, proxies=proxy_object, **kwargs)
                response_text = response.text
                response.close()
                return response_text
            return requests.get(url, **kwargs)

    @staticmethod
    def check_book_by_initial_link(book):
        initial_link = book.download_links[0]
        download_exists_in_storage_location = LibgenDatabase.check_for_duplicate_download_by_initial_link(initial_link=initial_link)
        return book if download_exists_in_storage_location is None else None

    def setup_download_path(self, search_term):
        try:
            self.download_path = config.get('libgen', 'download_path')
        except Exception:
            utilities.print_color("Could not find download path in config file, defaulting to './Downloads/'", 'red')
            self.download_path = "Downloads/"

        # Fix separator at end of folder for file creation
        if self.download_path[-1] != os.sep:
            self.download_path += os.sep

        download_path = self.download_path + config.get('libgen', 'search_downloads_path') + os.sep + search_term + os.sep

        # Create the folder if it doesn't exist
        self.download_path = utilities.create_path_if_not_exist(download_path)
