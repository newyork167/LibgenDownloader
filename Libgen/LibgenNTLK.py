import nltk
from nltk.corpus import wordnet

if __name__ == '__main__':
    # TODO: Get a list of synonyms to search with
    nltk.download('wordnet')

    synonyms = []
    antonyms = []

    for syn in wordnet.synsets("engineering"):
        for l in syn.lemmas():
            synonyms.append(l.name())
            if l.antonyms():
                antonyms.append(l.antonyms()[0].name())

    print(set(synonyms))
    print(set(antonyms))
