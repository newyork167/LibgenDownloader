from random import shuffle
from multiprocessing import Lock, Queue
from Utilities import utilities, configuration as config


# To make sure we use as many proxies as possible maxed by number of threads
available_proxy_queue = Queue()
available_proxies = config.get('proxy', 'proxy_urls').split(',')
shuffle(available_proxies)
[available_proxy_queue.put(x) for x in available_proxies]
available_proxy_queue.put(None)
available_proxy_queue_lock = Lock()


class LibGenProxy:
    proxy = {}
    proxy_url = ""
    safe_name = ""
    proxy_string = ""

    def __init__(self, no_proxy=False):
        global available_proxy_queue_lock

        if no_proxy:
            self.proxy = None
        else:
            available_proxy_queue_lock.acquire()
            try:
                self.proxy_username = config.get('proxy', 'proxy_username')
                self.proxy_password = config.get('proxy', 'proxy_password')
                self.proxy_port = config.get('proxy', 'proxy_port')

                # Get a random proxy URL and remove the proxy from the available list
                # Later we will re-add it
                self.proxy_url = available_proxy_queue.get()

                utilities.print_color("Got proxy: {}".format(self.proxy_url))
                # utilities.print_color("Remaining Proxy Queue: {}".format(available_proxy_queue.__dict__))

                if self.proxy_url is None:
                    raise Exception("Use Local IP")

                self.proxy_format_str = config.get('proxy', 'proxy_url')

                self.proxy_string = self.proxy_format_str.format(
                    username=self.proxy_username,
                    password=self.proxy_password,
                    url=self.proxy_url,
                    port=self.proxy_port
                )

                self.proxy['http'] = self.proxy_string
                self.proxy['https'] = self.proxy_string

                self.safe_name = self.proxy_string.split('@')[-1]
            except Exception as ex:
                if str(ex) != "Use Local IP":
                    utilities.print_color("Could not get proxy from config file")
                self.proxy = None
                self.proxy_string = None
            finally:
                available_proxy_queue_lock.release()

    def __del__(self):
        # utilities.print_color('Returning proxy: {} to queue'.format(self.proxy_url))
        # utilities.print_color("Proxy Queue: {}".format(available_proxy_queue.__dict__))
        available_proxy_queue.put(self.proxy_url)
