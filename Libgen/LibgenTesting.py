import requests
from Utilities import utilities, configuration as config


class LibgenTesting:
    @staticmethod
    def libgen_test_vpn_connections():
        proxy_username = config.get('proxy', 'proxy_username')
        proxy_password = config.get('proxy', 'proxy_password')
        proxy_port = config.get('proxy', 'proxy_port')

        test_url = config.get('libgen', 'libgen_url')

        for proxy_url in config.get('proxy', 'proxy_urls').split(','):
            proxy_format_str = config.get('proxy', 'proxy_url')

            proxy_string = proxy_format_str.format(username=proxy_username, password=proxy_password, url=proxy_url, port=proxy_port)
            proxy_object = {'http': proxy_string, 'https': proxy_string}

            try:
                requests.get(test_url, proxies=proxy_object)
                utilities.print_color("{} is good".format(proxy_url), 'green')
            except Exception:
                utilities.print_color("{} is bad".format(proxy_url), 'red')
