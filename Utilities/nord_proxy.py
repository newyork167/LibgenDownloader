from multiprocessing import Queue, Lock
from Utilities import utilities, configuration as config
from random import shuffle


class NordProxy:
    available_proxy_queue = Queue()
    available_proxy_queue_lock = Lock()

    def __init__(self):
        self.setup_proxy_queue()

    def get_next_available_proxy(self):
        if self.available_proxy_queue.empty():
            self.setup_proxy_queue()
        self.available_proxy_queue_lock.acquire()
        proxy = None

        try:
            proxy_username = config.get('proxy', 'proxy_username')
            proxy_password = config.get('proxy', 'proxy_password')
            proxy_port = config.get('proxy', 'proxy_port')

            # Get a random proxy URL and remove the proxy from the available list
            proxy_url = self.available_proxy_queue.get()

            if proxy_url is None:
                return None

            proxy_format_str = config.get('proxy', 'proxy_url')
            proxy_string = proxy_format_str.format(username=proxy_username, password=proxy_password, url=proxy_url, port=proxy_port)

            proxy = {'http': proxy_string, 'https': proxy_string}
        except Exception as ex:
            utilities.print_color("Could not get proxy from config file: {}".format(ex))
        finally:
            self.available_proxy_queue_lock.release()
            return proxy

    def return_proxy_to_queue(self, proxy):
        if proxy is None:
            self.available_proxy_queue.put(None)
        else:
            self.available_proxy_queue.put(proxy['http'])

    def setup_proxy_queue(self):
        available_proxies = config.get('proxy', 'proxy_urls').split(',')
        shuffle(available_proxies)
        [self.available_proxy_queue.put(x) for x in available_proxies]
        self.available_proxy_queue.put(None)
