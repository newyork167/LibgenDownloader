import datetime
import sys
from Libgen import LibgenDatabase
from Libgen.LibgenTesting import LibgenTesting
from Libgen.LibgenArgParser import LibgenArgParser
from Libgen.LibgenDownloader import LibgenDownloader


if __name__ == '__main__':
    arguments = LibgenArgParser().parse_arguments()

    start_time = datetime.datetime.now()
    libgen_downloader = LibgenDownloader(
        strict_search=arguments.libgen_strict_search,
        redownload_existing=arguments.libgen_redownload
    )
    successful_file_count = []

    if arguments.libgen_test:
        LibgenTesting.libgen_test_vpn_connections()
    elif arguments.libgen_fix_authors:
        LibgenDatabase.fix_authors()
    elif arguments.libgen_download_file:
        libgen_downloader.parse_file(libgen_file=arguments.libgen_download_file)
    elif arguments.libgen_search:
        successful_file_count = libgen_downloader.libgen_pool_search_replacement(
            search_term=arguments.libgen_search.title() if arguments.libgen_search[:5] != 'topic' else arguments.libgen_search,
            max_pages=arguments.libgen_max_pages if arguments.libgen_max_pages else 1,
            starting_page=arguments.libgen_starting_page if arguments.libgen_starting_page else 1)
    else:
        LibgenArgParser().print_help()
        sys.exit(3)

    end_time = datetime.datetime.now()

    print("It took {minutes} minutes to download {file_count} files".format(
        minutes=end_time - start_time,
        file_count=len([x for x in successful_file_count if x == libgen_downloader.DOWNLOAD_SUCCESSFUL]))
    )
