param (
    [Parameter(Mandatory=$true)][string]$search,
    [int]$pages = 3,
    [int]$starting_page = 1,
    [bool]$redownload = 0
 )

switch ($redownload)
{
    $true { .\venv\Scripts\python.exe .\main.py -s $search -n $pages --starting-page $starting_page --redownload; break }
    default { .\venv\Scripts\python.exe .\main.py -s $search -n $pages --starting-page $starting_page; break }
}